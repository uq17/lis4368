> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4368 - Advanced Web Applications Development

## Umar Qureshi

### Assignment 3 Requirements:

*Deliverables:*

1. Entity Relationship Diagram (ERD)
2. Include Data (at least 10 records each table)


#### README.md file should include the following items:

* Screenshot of ERD
* Links to ERD file and SQL script


#### Assignment Screenshot and Links:

*Screenshot A3 ERD*:

![A3 ERD](img/a3.png "ERD based upon A3 Requirements")

*A3 docs: a3.mwb and a3.sql*:

[A3 MWB File](docs/a3.mwb "A3 ERD in .mwb format")

[A3 SQL File](docs/a3.sql "A3 SQL Script")

