> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4368 - Advanced Web Applications Development

## Umar Qureshi

### Assignment 2 Requirements:

*Three-Parts:*

1. MySQL Installation
2. Compiling and Using Servlets
3. Database Connectivity Using Servlets

#### README.md file should include the following items:

* Screenshots of deployment of servlets
* Assessment links to servlets (querybook, sayhi, sayhello) 


#### Assignment Screenshots:

*Hello Home:*
[http://localhost:9999/hello](http://localhost:9999/hello "Hello Home")

![Hello Directory Screenshot](img/hellodirectory.png)

![Hello Index Screenshot](img/helloindex.png)

*HelloServlet:*
[http://localhost:9999/hello/sayhello](http://localhost:9999/hello/sayhello "invokes HelloServlet")

![HelloServlet Screenshot](img/using_servlets.png)

*AnotherHelloServlet:*
[http://localhost:9999/hello/sayhi](http://localhost:9999/hello/sayhi "invokes AnotherHelloServlet")

![Query Results Screenshot](img/sayhiservlet.png)

*Query Book:*
[http://localhost:9999/hello/querybook.html](http://localhost:9999/hello/querybook.html "invokes QueryServlet")

![Query Servlet Screenshot](img/database_connectivity1.png)

*Query Results*:

![Query Results Screenshot](img/database_connectivity2.png)



