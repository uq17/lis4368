# Advanced Web Applications Development

## Umar Qureshi

This project involves the development of a robust, dynamic web application running on Apache Tomcat Server. This 
application allows users to view customer information through a responsive table, enter information into a form that is 
validated both on the client and server side, and manipulate a backend database. Users are able to insert, update, and
delete data from the customer table, and these actions are then validated on the server side and applied to a backend MySQL
database. All webpages were created using .jsp pages and backend servlets were created using Java. View the project
requirements and screenshots of the application below. 

### Project Requirements:

1. Basic Server Side Validation
2. Thanks page with data displayed after successful validation
3. Data inserted into database through prepared statements to prevent SQL injection
4. Ability to insert, update, and delete data in connection with a database
5. Display data changes within webpage in form of responsive table

#### Project Screenshots:

*Valid User Form Entry*:

![Form Entry](img/entry.png)

*Successful Validation*:

![Successful Validation](img/thanks.png)

*Data Table Page*:

![Data Table](img/display.png)

*Updating Data Form*:

![Update](img/edit.PNG)

*Updated Data Table Page*:

![Updated](img/edited.png)

*Deleting Data with Delete Warning*:

![Deleted Data](img/delete.png)

*Associated Database Changes*:

![Database Changes](img/data.png)
![Database Changes](img/data2.png)

