> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4368 - Advanced Web Application

## Umar Qureshi

### Assignment 5 Requirements:

*Three Parts:*

1. Basic Server Side Validation
2. Thanks page with data displayed after successful validation
3. Data inserted into database through prepared statements to prevent SQL injection

#### README.md file should include the following items:

* Screenshot of valid user form entry
* Screenshot of successful validation
* Screenshot of associated database entry

#### Assignment Screenshots:

*Valid User Form Entry*:

![Form Entry](img/entry.png)

*Successful Validation*:

![Successful Validation](img/thanks.png)

*Associated Database Entry*:

![Database Entry](img/data.png)

