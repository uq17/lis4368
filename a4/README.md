> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4368 - Advanced Web Applications Development

## Umar Qureshi

### Assignment 4 Requirements:

*Deliverables:*

1. Basic Server Side Validation
2. Thanks page with data displayed after successful validation


#### README.md file should include the following items:

* Screenshot of passed validation
* Screenshot of failed validation


#### Assignment Screenshots:

*Failed Validation*:

![Failed Validation](img/failed.png)

*Successful Validation*:

![Successful Validation](img/passed.png)
