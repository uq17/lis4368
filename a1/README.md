> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4368 - Advanced Web Applications Development

## Umar Qureshi

### Assignment 1 Requirements:

*Three Parts:*

1. Distributed Version Control with Git and BitBucket 
2. Java/JSP/Servlet Development Installation
3. Chapter Questions (Chs 1-4)

#### README.md file should include the following items:

* Screenshot of running java Hello (#1 above); 
* Screenshot of running https://localhost:9999 (#2 above, Step #4(b) in tutorial);
* git commands w/ short descriptions
* BitBucket repo links: a) this assignment and b) the completed tutorial above (bitbucketstationlocations);

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init - initializes a new Git repository
2. git status - displays the state of the working directory/snapshot
3. git add - moves changes from the working directory to the staging area
4. git commit - Commits staged snapshot to the project history
5. git push - moves a local branch to the remote repository
6. git pull - downloads a branch from the remote repository and merges it to the current
7. git branch - allows for isolated development environments within repository

#### Assignment Screenshots:

*Screenshot of running java Hello*:

![JDK Installation Screenshot](img/jdk_install.PNG)

*Screenshot of running https://localhost:9999*:

![Tomcat Installation Screenshot](img/tomcat.PNG)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/uq17/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/uq17/myteamquotes/ "My Team Quotes Tutorial")
