> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4368 - Advanced Web Application

## Umar Qureshi

### Project 1 Requirements:

*Three Parts:*

1. HTML5 form controls
2. JQuery client-side validation of forms
3. Understanding of JQuery regular expressions

#### README.md file should include the following items:

* Screenshot of failed validation
* Screenshot of successful validation

#### Assignment Screenshots:

*Failed Validation*:

![Failed Validation](img/failed.PNG)

*Successful Validation*:

![Successful Validation](img/success.PNG)
