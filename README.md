> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS 4368 -  Advanced Web Application

## Umar Qureshi

### LIS 4368 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install JDK
    - Install Tomcat
	- Provide screenshots of installations
	- Create Bitbucket Repo
	- Complete Bitbuck Tutorials
	  (bitbucketstationlocations and myteamquotes)
	- Provide git command descriptions
	
2. [A2 README.md](a2/README.md "My A2 README.md file")
	- Install MySQL
	- Create a data-driven web-application
	- Develop and deploy servlets
	- Provide screenshot of database connectivity

3. [A3 README.md](a3/README.md "My A3 README.md file")
	- Create ERDs using MySQL workbench
	- Include data in each ERD table
	- Forward-engineer ERD to create SQL script

4. [A4 README.md](a4/README.md "My A4 README.md file")
	- Utilize JSPs and Servlets for basic server side data validation
	- Set up framework for a database-connected user form

5. [A5 README.md](a5/README.md "My A5 README.md file")
	- Incorporate ability to insert data from user form to database
	- Use prepared statements to prevent SQL injection

6. [P1 README.md](p1/README.md "My P1 README.md file")
	- Add form controls in HTML to match attributes of customer entity
	- Add JQuery form validations and regular expressions to forms

7. [P2 README.md](p2/README.md "My P2 README.md file")
	- Incorporate ability to insert, update, and delete data from user form and table to database
	- Use prepared statements to prevent SQL injection
	- Reflect database changes in responsive data table within webapp


